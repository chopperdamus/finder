// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <sstream>

using namespace std;

int print_usage(const char *name) {
    cerr << "Usage: " << name << "<inputFileName>" << endl;
    return EXIT_FAILURE;
}

void term(int _code) {
    cout << endl << "Press any key to exit...";
    if (_code != 0) cin.get();
    exit(_code);
}

string getFilename(const char *prefix, const char *_inFile) {
    string ret;
    string id(_inFile);
    
    string::size_type n = id.find(".");
    if (n != string::npos) {
        id = id.substr(0, n);
    }
    
    ret.append(prefix);
    ret.append(id);
    ret.append(".txt");
    
    return ret;
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        term(print_usage(argv[0]));
    }

    ifstream input(argv[1]);
    if (!input.is_open()) {
        cerr << "Cannot open input file " << argv[1] << endl;
        term(EXIT_FAILURE);
    }

    ofstream up, down;
    up.open(getFilename("lead_", argv[1]));
    down.open(getFilename("trail_", argv[1]));

    vector<double> times;
    vector<int> sensor1;
    vector<int> sensor2;

    string line;
    while (getline(input, line)) {
        double time = 0;
        int s1 = 0, s2 = 0;

        sscanf(line.c_str(), "%lf,%d,%d", &time, &s1, &s2);

        times.push_back(time);
        sensor1.push_back(s1);
        sensor2.push_back(s2);
    }
    input.close();

    vector<int> *sFirst = &sensor1;
    vector<int> *sSecond = &sensor2;

    // 00 cont; 01 swap; 10 break; 11 break;
    // Use the sensor with the earliest '1' as leading
    for (vector<int>::const_iterator i = sensor1.begin(), j = sensor2.begin(); i != sensor1.end() && j != sensor2.end(); i++, j++) {
        if (*i == 1) {
            break;
        } else if (*i == 0 && *j == 1) {
            sSecond = &sensor1;
            sFirst = &sensor2;
            break;
        }
    }

    // 0 to 1
    int i = 1, j;
    while (i < sFirst->size()) {
        if (sFirst->at(i) == 1 && sFirst->at(i - 1) == 0 && sSecond->at(i-1) == 0) {
            j = i;
            while (sSecond->at(j) != 1 && j < sSecond->size()) j++;
            up << (times[j] - times[i]) * 1000 << endl;
            i = j + 1;
        }
        else {
            i++;
        }
    }
    up.close();

    // 1 to 0
    i = 1;
    while (i < sFirst->size()) {
        if (sFirst->at(i) == 0 && sFirst->at(i-1) == 1 && sSecond->at(i-1) == 1) {
            j = i;
            while (sSecond->at(j) != 0 && j < sSecond->size()) j++;
            down << (times[j] - times[i]) * 1000 << endl;
            i = j + 1;
        }
        else {
            i++;
        }
    }
    down.close();
    return 0;
}

